#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <ArduinoJson.h>
#include "AsyncTelegram.h"
#include <config.h>

float temperature = 0.0;
float humidity = 0.0;
unsigned long sensor_lasttime; // last time the sensors values were retrieved
unsigned long last_warning_at = 0;
unsigned int reminder_interval = REMINDER_INTERVAL * 60000;
unsigned long snoozed_until = 0;

const unsigned long BOT_MTBS = 2000; // mean time between message scans
const unsigned long SENSOR_MTBS = 2000; // mean time between sensor scans

DHT dht(DHTPIN, DHTTYPE);

AsyncTelegram bot;

void bot_setup()
{
  const String commands = F("["
                            "{\"command\":\"help\",  \"description\":\"Get bot usage help\"},"
                            "{\"command\":\"start\", \"description\":\"Message sent when you open a chat with a bot\"},"
                            "{\"command\":\"snooze\", \"description\":\"Snooze humidity alerts for 15 minutes\"},"
                            "{\"command\":\"status\",\"description\":\"Get current temperature and humidity\"}" // no comma on last command
                            "]");
  //bot.setMyCommands(commands);
}

void handleNewMessage(TBMessage new_message)
{
  String answer;

  Serial.println("Received " + new_message.text);
  //bot.sendChatAction(msg.chat_id, "typing");

  if (new_message.text == "/help")
    answer = "So you need _help_, uh? me too! use /start or /status";
  else if (new_message.text == "/start")
    answer = "Welcome my new friend! You are the first " + String(new_message.sender.username) + " I've ever met";
  else if (new_message.text == "/snooze") {
    answer = "Hope you know what you're doing...";
    snoozed_until = millis() + 60000 * 15;
  }
  else if (new_message.text == "/status")
    answer = "Temperature: " + String(temperature) + "°C\nHumidity: " + String(humidity) + "%";
  else
    answer = "Say what?";

  bot.sendToUser(new_message.sender.id, answer);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.printf("Free heap: %d\n", ESP.getFreeHeap());

  // attempt to connect to Wifi network:
  Serial.print("Connecting to Wifi SSID ");
  Serial.print(WIFI_SSID);
  WiFi.setAutoConnect(true);   
	WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.print("\nWiFi connected. IP address: ");
  Serial.println(WiFi.localIP());

  // Set the Telegram bot properies
  if(bot.updateFingerPrint())
      Serial.println("Telegram fingerprint updated");
  bot.setUpdateTime(BOT_MTBS);
  bot.setTelegramToken(BOT_TOKEN);
  
  // Check if all things are ok
  Serial.print("\nTest Telegram connection... ");
  bot.begin() ? Serial.println("OK") : Serial.println("NOK");
  
  Serial.print("Bot name: @");	
  Serial.println(bot.userName);

  // start taking temperature/moisture
  dht.begin();
}

void loop() {
  // put your main code here, to run repeatedly:

  // get sensor values every 2 seconds
  if (millis() - sensor_lasttime > SENSOR_MTBS) {  
    temperature = dht.readTemperature();
    humidity = dht.readHumidity();
    Serial.println(temperature);
    Serial.println(humidity);

    sensor_lasttime = millis();
  }

  // a variable to store telegram message data
	TBMessage msg;

  while (bot.getNewMessage(msg)){
    {
      Serial.println("Received message");
      handleNewMessage(msg);
    }
  }

  // check if still snoozed
  if (millis() > snoozed_until)
    snoozed_until = 0;

  String answer = "The humidity is getting dangerously high!\nPlease open a window.";

  if (snoozed_until == 0) {
    // send a message if humidity is too high and we have not sent a message in the last X minutes (specified by reminderInterval)
    if (humidity > HUMIDITY_THRESHOLD && ((last_warning_at > 0 && ((millis() - last_warning_at) > reminder_interval)) || last_warning_at == 0)) {
      Serial.println("Sending humidity alert!");
      for (int32_t chat_id: CHAT_IDS)
      {
        bot.sendToUser(chat_id, answer);
      }
      last_warning_at = millis();
    }
  }
}