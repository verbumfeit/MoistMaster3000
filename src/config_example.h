// Wifi network station credentials
#define WIFI_SSID "YOUR_WIFI_SSID"
#define WIFI_PASSWORD "YOUR_WIFI_PASSWORD"

// Telegram BOT Token (Get from Botfather)
#define BOT_TOKEN "XXXXXXXXXX:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

// Use @myidbot (IDBot) to find out the chat ID of an individual or a group
// Also note that you need to click "start" on a bot before it can
// message you
const int CHAT_IDS[] = {1010101010, 6969696969};

// DHT Sensor config
#define DHTPIN     5         // Digital pin connected to the DHT sensor
#define DHTTYPE    DHT22     // DHT 22 (AM2302)

#define HUMIDITY_THRESHOLD   59     // Send a Telegram message at X % relative humidity
#define REMINDER_INTERVAL    3      // Send a Telegram message every X minutes while humidity threshold is reached