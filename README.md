# MoistMaster3000

Arduino based temperature and humidity monitoring bot that sends Telegram messages if a ceratin humidity threshold is reached.

Rename config_example.h to config.h

Hook up the sensor like shown in this tutorial: https://randomnerdtutorials.com/complete-guide-for-dht11dht22-humidity-and-temperature-sensor-with-arduino/
and select the appropriate PIN in config.h

Create a Telegram Bot via The Botfather and set its HTTP API Key, your chat ID and your WiFi credentials in config.h